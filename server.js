const express = require('express')
const app = express()
const bodyParser = require('body-parser');

const LanguageTranslatorV3 = require('ibm-watson/language-translator/v3');
const { IamAuthenticator } = require('ibm-watson/auth');

const env = require('dotenv').config();

const fs = require('fs');
const TextToSpeechV1 = require('ibm-watson/text-to-speech/v1');
//const { IamAuthenticator } = require('ibm-watson/auth');

const textToSpeech = new TextToSpeechV1({
  authenticator: new IamAuthenticator({ apikey: process.env.SPEECH_API_KEY }),
  url: 'https://stream.watsonplatform.net/text-to-speech/api'
});


const languageTranslator = new LanguageTranslatorV3({
  authenticator: new IamAuthenticator({ apikey:  process.env.API_KEY }),
  url: 'https://gateway.watsonplatform.net/language-translator/api/',
  version: '2019-11-29',
});

app.use(bodyParser.urlencoded({ extended: true }));

app.set('view engine', 'ejs')

app.use(express.static('public'));

app.get('/', function (req, res) {
  res.render('index', {resultat: null, error: null});
})

app.post('/', function (req, res)  {

  if(req.body.langueO == req.body.langueT) {

    res.render('index', {resultat: null, error: 'Les deux langues ne doivent pas être identique'});
  } else {

 languageTranslator.translate(
    {
      text: req.body.original,
      source: req.body.langueO,
      target: req.body.langueT
    
    }).then(response => { 

        const param = {
          text: JSON.stringify(response.result.translations[0].translation),
          accept: 'audio/wav'
        };

        const synthStream = textToSpeech.synthesizeUsingWebSocket(param);
        synthStream.pipe(fs.createWriteStream('./audio.ogg'));

      res.render('index', {resultat: JSON.stringify(response.result.translations[0].translation), error: null});
    }).catch(err => {
        res.render('index', {resultat: null, error: 'Corriger svp'});
    });

  }

});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
});